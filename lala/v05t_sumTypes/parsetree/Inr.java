package lala.v05t_sumTypes.parsetree;

import lala.core.syntaxtree.Term;
import lala.v05t_sumTypes.parsetree.types.SumType;

public class Inr extends Term {

	public SumType type;
	public Term term;
	
	public Inr() {
		super();
	}

	public Inr(Term t) {
		super();
		this.term = t;
	}

	public Term replaceFreeVariable(String v, Term replacement) {
		term.replaceFreeVariable(v, replacement);
		return this;
	}
	
	

}
