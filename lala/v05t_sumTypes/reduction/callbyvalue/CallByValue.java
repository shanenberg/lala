package lala.v05t_sumTypes.reduction.callbyvalue;

import lala.core.syntaxtree.Term;
import lala.v05t_sumTypes.parsetree.Case;
import lala.v05t_sumTypes.parsetree.Inl;
import lala.v05t_sumTypes.parsetree.Inr;

public class CallByValue extends lala.v03t_boolsAndInts.reduction.callbyvalue.CallByValue {
	
	public Term reduce(Inl t) {
		t.term = reduce(t.term);
		return t;
	}
	
	public Term reduce(Inr t) {
		t.term = reduce(t.term);
		return t;
	}
	
	public Term reduce(Case c) {
		if (isReducible(c.term)) {
			c.term = reduce(c.term);
			return c;
		} else if (c.term instanceof Inl) {
			return c.inlTerm.replaceFreeVariable(c.inlVar, ((Inl)c.term).term);
		} else if (c.term instanceof Inr) {
			return c.inrTerm.replaceFreeVariable(c.inrVar, ((Inr)c.term).term);
		}
		return c;
	}	

	public Boolean isReducible(Term t) {
		return new IsReducibleCallByValue().isReducible(t);
	}	
}
