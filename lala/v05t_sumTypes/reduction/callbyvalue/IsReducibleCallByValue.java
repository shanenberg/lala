package lala.v05t_sumTypes.reduction.callbyvalue;

import lala.v05t_sumTypes.parsetree.Case;
import lala.v05t_sumTypes.parsetree.Inl;
import lala.v05t_sumTypes.parsetree.Inr;


public class IsReducibleCallByValue 
	extends 
	lala.v04t_if_simple.reduction.callbyvalue.IsReducibleCallByValue {
	
	public Boolean isReducible(Inl t) {
		return isReducible(t.term);
	}
	
	public Boolean isReducible(Inr t) {
		return isReducible(t.term);
	}	
	
	public Boolean isReducible(Case c) {
		return isReducible(c.term) || 
				(c.term instanceof Inl) || (c.term instanceof Inr);
	}		
	

}
