package lala.v02_typed_simple.reduction.callbyvalue;

import lala.core.reduction.ReductionStrategy;
import lala.core.syntaxtree.Term;
import lala.v02_typed_simple.parser.parsetree.Abstraction;

public class CallByValue extends lala.v01_untyped_simple.reduction.callbyvalue.CallByValue 
	implements ReductionStrategy {
	
	public Term reduce(Abstraction a) {
		return a;
	}	
	
	public Boolean isReducible(Term t) {
		return new IsReducibleCallByValue().isReducible(t);
	}	
}
