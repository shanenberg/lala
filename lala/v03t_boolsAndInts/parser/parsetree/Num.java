package lala.v03t_boolsAndInts.parser.parsetree;

import lala.core.syntaxtree.ExpressionConstant;
import lala.core.syntaxtree.Term;


public class Num extends ExpressionConstant {
	public int value;

	public Num() {}
	public Num(int i) {value=i;}

	@Override
	public Term replaceFreeVariable(String v, Term replacement) {
		return this;
	}

	@Override
	public String toString() {
		return "(" + String.valueOf(value) + ")";
	}

	
}
