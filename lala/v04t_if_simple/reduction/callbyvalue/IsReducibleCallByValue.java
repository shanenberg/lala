package lala.v04t_if_simple.reduction.callbyvalue;

import lala.core.syntaxtree.Application;
import lala.v04t_if_simple.parser.parsetree.If;


public class IsReducibleCallByValue extends lala.v03t_boolsAndInts.reduction.callbyvalue.IsReducibleCallByValue {
	
	public Boolean isReducible(If t) {
		return true;
	}
	
	public Boolean isReducible(Application a) {
		return super.isReducible(a);
	}	
	

}
