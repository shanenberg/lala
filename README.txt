Languages
=========

The package lala (="Lambda Language") contains Java implementations of different lambda languages. The languages are motivated by the description of Benjamin Pierce's book "Types and programming languages" (MIT Press).


Every language consists of 
- a parser (a really nasty implementation of a parser, just to make testing a little bit more convenient)
- an evaluation strategy (such as call by value of normal order)
- in case it is a statically typed language, an additional type checker

Language 01: v01_untyped_simple = Simply and untyped lambda calculus
====================================================================
Contains a simple, untyped lambda calculus. The language consists of variables, abstractions, and applications.

syntax:
  Term t = (x) | (t1 t2) | (λx.t)

example programs:
 (x), ((x)(y)), (λx.(x)), ((x)((y)(z))), (λx.(λy.(y)))

Evaluation strategies:
  Call by value
  Normal order

Language 02: v02_typed_simple = Simply typed lambda calculus
============================================================
Simply typed lambda calculus, consisting (still) of variables, abstractions, and applications.

Additionally, it contains the three types: Bool, Num and Function. The language does not contain so far any expression constants.

syntax:
  Term t = (x) | (t1 t2) | (λx:T.t)
  Type T = Bool | Num | T1->T2

Example programs:
  - (λx:Bool.(x))
  - (λx:(Bool->Bool).(x))
  - (λx:(Bool->Num).(x))

Language 03: v03t_boolsAndInts = Simply typed lambda calculus with ints and bools
=================================================================================
 
 
 
